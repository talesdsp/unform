import {Scope} from "@unform/core";
import {Form} from "@unform/web";
import React, {useEffect, useRef} from "react";
import * as Yup from "yup";
import "./App.css";
import Input from "./components/Form/input";

function App() {
  const formRef = useRef(null);

  useEffect(() => {
    setTimeout(() => {
      formRef.current.setData({
        name: "tales",
        email: "tales@gmail.com",
        address: {
          city: "rio"
        }
      });
    }, 2000);
  }, []);

  async function handleSubmit(data, {reset}) {
    try {
      const schema = Yup.object().shape({
        name: Yup.string().required("name is required"),
        email: Yup.string()
          .email("email is not valid")
          .required("email is required"),
        address: Yup.object().shape({
          city: Yup.string()
            .min(3, "min 3 char")
            .required("city is required")
        })
      });

      await schema.validate(data, {
        abortEarly: false
      });

      reset();
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};
        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
    }
  }

  return (
    <main>
      <h1>Hello World</h1>
      {/* initialData={initialData} for local data */}
      <Form ref={formRef} onSubmit={handleSubmit}>
        <Input label="Name" name="name" />
        <Input label="Email" name="email" />
        <Input label="Password" type="password" name="password" />

        <Scope path="address">
          <Input label="Street" name="street" />
          <Input label="City" name="city" />
          <Input label="Number" name="number" />
          <Input label="State" name="state" />
        </Scope>

        <button type="submit">Send</button>
      </Form>
    </main>
  );
}

export default App;
